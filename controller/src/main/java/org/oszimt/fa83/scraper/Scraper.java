package org.oszimt.fa83.scraper;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.oszimt.fa83.exception.BotDetectionCookieInvalid;
import org.oszimt.fa83.pojo.ScrapeQuery;
import org.oszimt.fa83.pojo.ScrapeResultPojo;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * scraping class for scraping results from ImmoScout.
 */
public class Scraper {

    public Scraper() {
    }

    /**
     * scrape Immoscout with query details from given query.
     *
     * @param query query to scrape immoscout for.
     * @return result of query.
     * @throws IOException
     */
    public List<ScrapeResultPojo> scrape(ScrapeQuery query, String token) throws IOException, BotDetectionCookieInvalid {
        WebClient client = new WebClient(BrowserVersion.FIREFOX);
        Cookie cookie = new Cookie(".immobilienscout24.de", "reese84", token);
        client.getCookieManager().addCookie(cookie);
        client.getOptions().setRedirectEnabled(true);
        client.getOptions().setPopupBlockerEnabled(false);
        client.getOptions().setDoNotTrackEnabled(true);
        client.getOptions().setActiveXNative(true);
        client.getOptions().setWebSocketEnabled(true);
        client.getOptions().setWebSocketEnabled(true);
        client.getOptions().setCssEnabled(true);
        client.getOptions().setConnectionTimeToLive(100000);
        client.getOptions().setJavaScriptEnabled(true);
        client.getCookieManager().setCookiesEnabled(true);
        client.waitForBackgroundJavaScript(60000);
        client.getOptions().setThrowExceptionOnScriptError(false);
        client.getOptions().setThrowExceptionOnFailingStatusCode(false);

        List<String> resultIdList = new ArrayList<>();
        List<ScrapeResultPojo> resultList = new ArrayList<>();
        HtmlPage page = client.getPage(query.toUrl());

        if (page.getElementById("explanation") != null) {
            throw new BotDetectionCookieInvalid();
        }
        List<HtmlElement> listCollection = page.getDocumentElement().getElementsByAttribute("ul", "id", "resultListItems");
        for (HtmlElement list : listCollection) {
            for (HtmlElement element : list.getHtmlElementDescendants()) {
                if (element.hasAttribute("data-id")) {
                    String oid = element.getAttribute("data-id");
                    resultIdList.add(oid);
                }
            }
        }
        for (String oid : resultIdList) {
            String url = "https://www.immobilienscout24.de/expose/" + oid;
            ScrapeResultPojo result = new ScrapeResultPojo(url, oid);
            resultList.add(result);
        }
        client.close();
        return resultList;
    }


}
