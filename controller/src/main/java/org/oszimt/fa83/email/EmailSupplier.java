package org.oszimt.fa83.email;

import java.util.List;

/**
 * singleton class for central access to current email
 */
public class EmailSupplier {

    private static List<String> email;
    private static final EmailSupplier INSTANCE = new EmailSupplier();

    private EmailSupplier(){
        //hide constructor
    }

    public static EmailSupplier getInstance(){
        return INSTANCE;
    }

    public static List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }
}
