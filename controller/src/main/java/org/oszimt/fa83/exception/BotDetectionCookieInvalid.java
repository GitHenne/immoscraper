package org.oszimt.fa83.exception;

public class BotDetectionCookieInvalid extends Exception {

    private static final String COOKIE_INVALID = "Bot detection Cookie ungültig";

    @Override
    public String getMessage() {
        return COOKIE_INVALID;
    }
}
