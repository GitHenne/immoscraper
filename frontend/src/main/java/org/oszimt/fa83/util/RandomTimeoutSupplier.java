package org.oszimt.fa83.util;

public final class RandomTimeoutSupplier {

    private static long random;

    private RandomTimeoutSupplier() {
    }

    public static long getCurrentTimeout(){
        return random;
    }

    public static long getNewTimeout(){
        random = getRandomLong();
        return random;
    }

    private static long getRandomLong(){
        long leftLimit = 21000L;
        long rightLimit = 40000L;
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }
}
